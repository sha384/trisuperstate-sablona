// import "./modules/HelloWorld";

// Some cool modules
import "./modules/Animate";
import "./modules/ToggleBodyClass";
import "./modules/ScrollClass";
import "./modules/Dropdown";
import "./modules/Carousel";
import "./modules/Menu";
import "./modules/CloseAlert";
// import "./modules/Tabs";
import "./modules/ShowPass";
import "./modules/Fancybox";
import "./modules/Select";
import "./modules/CreditCard";
import "./modules/HomepageCarousel";
import "./modules/Copy";
