import Splide from '@splidejs/splide';

const EL = '.js-homepage-carousel';
const ACTIVE_CLASS = 'is-active';
const ELS = document.querySelectorAll(EL);

ELS.forEach((EL) => {
  const splide = new Splide(EL, {
    type: 'fade',
    pagination: true,
    arrows: false,
  }).mount();

  const videos = EL.querySelectorAll('video');

  function goToNextSlide() {
    splide.go('>');
  }

  splide.on('active', function (Slide) {
    const video = Slide.slide.querySelector('video');
    if (video) {
      video.currentTime = 0;
      video.play();

      video.addEventListener('ended', goToNextSlide);
    }
  });

  splide.on('inactive', function (Slide) {
    const video = Slide.slide.querySelector('video');
    if (video) {
      video.pause();
      video.currentTime = 0;

      video.removeEventListener('ended', goToNextSlide);
    }
  });

  splide.on('mounted', function () {
    const firstSlide = splide.Components.Slides.getAt(0);
    if (firstSlide) {
      const firstVideo = firstSlide.slide.querySelector('video');
      if (firstVideo) {
        firstVideo.currentTime = 0;
        firstVideo.play();

        firstVideo.addEventListener('ended', goToNextSlide);
      }
    }
  });
});
