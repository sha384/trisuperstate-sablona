document.addEventListener("DOMContentLoaded", function() {
  function copyToClipboard() {
      var copyText = document.getElementById("referralLink");

      if (copyText) {
          copyText.disabled = false;
          copyText.select();
          copyText.setSelectionRange(0, 99999);

          document.execCommand("copy");

          copyText.disabled = true;

          alert("Odkaz zkopírován: " + copyText.value);
      } else {
          console.error("Prvek s ID 'referralLink' neexistuje.");
      }
  }
  var copyButton = document.querySelector('.copy');
  if (copyButton) {
      copyButton.onclick = copyToClipboard;
  } else {

  }
});
