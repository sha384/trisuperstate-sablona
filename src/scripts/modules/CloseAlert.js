function closeAlert(alertElement) {
  const interval = parseInt(alertElement.getAttribute('data-interval'));
  setTimeout(() => {
    alertElement.classList.add('alert--hidden');
  }, interval);
}

const alerts = document.querySelectorAll('.alert');

alerts.forEach(alert => {
  const dataInterval = alert.getAttribute('data-interval');
  if (dataInterval) {
    closeAlert(alert);
  }
});
