document.addEventListener('DOMContentLoaded', function() {
	const formItems = document.querySelectorAll('.form__item');

	formItems.forEach(formItem => {
		const showPassCheckbox = formItem.querySelector('.showPass');
		if (!showPassCheckbox) return;

		showPassCheckbox.addEventListener('change', function() {
			formItem.classList.toggle('is-visible', this.checked);

			if (this.checked) {
				const textInput = formItem.querySelector('input[type="password"]');
				if (textInput) {
					textInput.type = 'text';
				}
			} else {
				const textInput = formItem.querySelector('input[type="text"]');
				if (textInput) {
					textInput.type = 'password';
				}
			}
		});
	});
});
