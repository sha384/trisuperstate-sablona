import { Fancybox } from "@fancyapps/ui";

Fancybox.bind("[data-fancybox]", {

});

if (document.getElementById("openModal")) {
  document.getElementById("openModal").addEventListener("click", () => {
    Fancybox.show([
      {
        src: "#dialog-on-js",
        type: "inline",
      }
    ]);
  });
}
