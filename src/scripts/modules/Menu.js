const openers = document.querySelectorAll('.header-nav__opener');

openers.forEach(opener => {
    opener.addEventListener('click', () => {
        const parentLi = opener.closest('li');

        const isOpen = parentLi.classList.contains('is-open');

        if (isOpen) {
            parentLi.classList.remove('is-open');
        } else {
            parentLi.classList.add('is-open');
        }
    });
});
