document.addEventListener("DOMContentLoaded", function() {
    // Funkce pro skrytí všech obsahových tabů
    function hideAllContentTabs() {
        document.querySelectorAll('.js-tabs-list > div').forEach(function(tabContent) {
            tabContent.style.display = 'none';
        });
    }

    // Funkce pro zobrazení obsahového tabu odpovídajícího vybranému navigačnímu tabu
    function showContentTab(tabId) {
        hideAllContentTabs();
        console.log('Showing tab:', tabId);
        var contentTab = document.querySelector('#' + tabId);
        if (contentTab) {
            contentTab.style.display = 'block';
        }
    }

    // Funkce pro skrytí všech submenu
    function hideAllSubmenus() {
        document.querySelectorAll('.js-tabs-nav .submenu').forEach(function(submenu) {
            submenu.style.display = 'none';
        });
    }

    // Funkce pro zobrazení submenu pro vybraný nadřazený li
    function toggleSubmenu(navTab) {
        var submenu = navTab.querySelector('.submenu');
        if (submenu) {
            if (submenu.style.display === 'block') {
                submenu.style.display = 'none';
            } else {
                submenu.style.display = 'block';
            }
        }
    }

    // Funkce pro přepínání třídy 'current' na aktivní navigační položku
    function toggleCurrentClass(navTab) {
        document.querySelectorAll('.js-tabs-nav > li').forEach(function(tab) {
            tab.classList.remove('current');
        });
        navTab.classList.add('current');
    }

    // Funkce pro odebrání třídy 'current' ze všech položek submenu
    function removeCurrentClassFromSubmenu() {
        document.querySelectorAll('.js-tabs-nav .submenu > li').forEach(function(submenuItem) {
            submenuItem.classList.remove('current');
        });
    }

    // Přidání posluchačů událostí na navigační taby
    document.querySelectorAll('.js-tabs-nav > li').forEach(function(navTab) {
        navTab.addEventListener('click', function(event) {
            event.preventDefault();
            var tabId = navTab.dataset.tabId;
            console.log('Clicked tab:', tabId);
            showContentTab(tabId + '-content');
            hideAllSubmenus(); // Skryje všechna submenu
            toggleSubmenu(navTab); // Zobrazí nebo skryje submenu pro aktuální navTab
            toggleCurrentClass(navTab); // Přepne třídu 'current' na aktivní navigační položku
            removeCurrentClassFromSubmenu(); // Odebere třídu 'current' ze všech položek submenu
        });

        // Přidání posluchačů událostí na odkazy v podmenu
        navTab.querySelectorAll('.submenu > li').forEach(function(submenuTab) {
            submenuTab.addEventListener('click', function(event) {
                event.stopPropagation(); // Zabraňuje probublávání události k nadřazenému <li>
                var tabId = submenuTab.dataset.tabId;
                console.log('Clicked submenu tab:', tabId);
                showContentTab(tabId + '-content');
                removeCurrentClassFromSubmenu(); // Odebere třídu 'current' ze všech položek submenu
                submenuTab.classList.add('current'); // Přidá třídu 'current' ke kliknutému submenu
            });
        });
    });

    // Zobrazení výchozího obsahového tabu
    showContentTab('tab1-content');
});
