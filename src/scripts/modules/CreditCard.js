// Masking function for credit card number
let cardNumberElement = document.getElementById('card-number');
if (cardNumberElement) {
    cardNumberElement.addEventListener('input', function(e) {
        let value = e.target.value.replace(/\D/g, '');
        e.target.value = value.replace(/(\d{4})(?=\d)/g, '$1 ');
    });
}

// Masking function for expiry date
let expiryDateElement = document.getElementById('expiry-date');
if (expiryDateElement) {
    expiryDateElement.addEventListener('input', function(e) {
        let value = e.target.value.replace(/\D/g, '');
        if (value.length >= 3) {
            value = value.replace(/(\d{2})(\d{1,2})/, '$1/$2');
        }
        e.target.value = value;
    });
}

// Masking function for CVC
let cvcElement = document.getElementById('cvc');
if (cvcElement) {
    cvcElement.addEventListener('input', function(e) {
        e.target.value = e.target.value.replace(/\D/g, '');
    });
}
