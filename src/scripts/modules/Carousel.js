import Splide from '@splidejs/splide';

const EL = '.js-carousel'
const ACTIVE_CLASS = 'is-active'
const ELS = document.querySelectorAll(EL);

ELS.forEach((EL) => {
  new Splide(EL, {
    pagination: true,
    arrows: false,
    gap: 30,
    perMove: 1,
    perPage: 5,
    breakpoints: {
      1024: {
        perPage: 4,
      },
      768: {
        perPage: 3,
      },
      500: {
        perPage: 2,
      }
    }
  }).mount();
});
